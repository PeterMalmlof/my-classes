unit TMainClassesUnit;

  // TGenTreeViewUnit

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,

  TGenAppPropUnit,
  TGenClassesFrameUnit;

type
  TMainClasses = class(TForm)
    GenClassesFrame1: TGenClassesFrame;
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    objMainRect : TGenAppPropRect;

  public
    { Public declarations }
  end;

var
  MainClasses: TMainClasses;

implementation

{$R *.dfm}

Uses
  TGenLogUnit,
  TGenStrUnit,
  TGenTextFileUnit,
  TGenClassesUnit;

//------------------------------------------------------------------------------
//  Create Form
//------------------------------------------------------------------------------
procedure TMainClasses.FormCreate(Sender: TObject);
var
  sPath : string;
begin
  TheLog.Log('TMainClasses.FormCreate');
  //----------------------------------------------------------------------------
  // Add Search Folders for Modules
  //----------------------------------------------------------------------------

  sPath := ExDelim(GetFolderName(Application.ExeName));
  TGenClasses.AddSearchPath(sPath);

  sPath := ExDelim(GetFolderName(sPath));
  TGenClasses.AddSearchPath(sPath + '\TGenUnits');
  TGenClasses.AddSearchPath(sPath + '\MediaDb');
  TGenClasses.AddSearchPath(sPath + '\My Explorer');

  // Create MainRect Application Property and add to TheAppProps

  objMainRect := TGenAppPropRect.Create(
        'Classes','Rect', Rect(100,100,800,500));
  TheAppProps.AddProp(objMainRect);

  objMainRect.SetBounds(self);

  GenClassesFrame1.StartUp;

  TheLog.Log(StrStuff('TMainClasses.Started','-',70));
  TheLog.Log('');
end;
//------------------------------------------------------------------------------
//  Destroy Form
//------------------------------------------------------------------------------
procedure TMainClasses.FormDestroy(Sender: TObject);
begin
  TheLog.Log('');
  TheLog.Log(StrStuff('TMainClasses.Closing','-',70));

  // Get MainRect

  objMainRect.GetBounds(self);
end;
//------------------------------------------------------------------------------
//  ResizeForm
//------------------------------------------------------------------------------
procedure TMainClasses.FormResize(Sender: TObject);
begin
  TheLog.Log('');
  TheLog.Log('TMainClasses.FormResize');
  GenClassesFrame1.Left   := 0;
  GenClassesFrame1.Width  := self.ClientWidth;
  GenClassesFrame1.Top    := 0;
  GenClassesFrame1.Height := self.ClientHeight;
end;
//------------------------------------------------------------------------------
//                           INITIALIZATION/FINALIZATION
//------------------------------------------------------------------------------
initialization
  TheLog.Log('TMainClasses.initialization');
  RegClass(TMainClasses);
finalization
  TheLog.Log('TMainClasses.finalization');
end.
