object MainClasses: TMainClasses
  Left = 262
  Top = 733
  Width = 405
  Height = 285
  Caption = 'MainClasses'
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = 'Verdana'
  Font.Style = [fsItalic]
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 14
  inline GenClassesFrame1: TGenClassesFrame
    Left = -6
    Top = -7
    Width = 311
    Height = 258
    TabOrder = 0
    inherited PageControl: TPageControl
      inherited ClassesTab: TTabSheet
        inherited ClassesSum: TLabel
          Width = 74
          Height = 14
        end
      end
      inherited ModulesTab: TTabSheet
        inherited ModulesSum: TLabel
          Width = 77
          Height = 14
        end
        inherited LogLabel: TLabel
          Width = 52
          Height = 14
        end
      end
    end
  end
end
